/**
 * Created by vietanha34 on 10/31/14.
 */
var tv4 = require('tv4');
var fs = require('fs');
var utils = require('../util/utils');
var logger = require('pomelo-logger').getLogger('pomelo', __filename);

module.exports = function (app, opts) {
  return new Component(app, opts);
};

var Component = function (app, opts) {
  this.app = app;
  this.opts = opts || {};
  this.jsonSchema = {};
  try {
    this.loadConfig();
  }catch (err){
    logger.error(err);
  }
};

var pro = Component.prototype;

pro.name = '__validate__';

pro.loadConfig = function () {
  var path = this.app.getBase() + '/config/validation.json';
  if (fs.existsSync(path)) {
    this.jsonSchema = require(path);
  }
};

pro.validate = function (msg, route) {
  if (this.jsonSchema[route]) {
    return tv4.validateMultiple(msg, this.jsonSchema[route])
  }else{
    return {valid : true};
  }
};





